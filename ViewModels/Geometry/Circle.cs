﻿using System.Drawing;

namespace tsp.ViewModels.Geometry
{
    public class Circle : Drawable
    {
        public Circle(Point center, int radius)
        {
            Top = center.Y - radius;
            Left = center.X - radius;
            Radius = radius;
        }

        public int Radius { get; protected set; }
        public int Width => Radius * 2;
        public int Height => Radius * 2;
    }
}
