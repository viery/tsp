﻿using System.Drawing;

namespace tsp.ViewModels.Geometry
{
    public class Line : Drawable
    {
        public Line(Point start, Point end)
        {
            Top = 0;
            Left = 0;

            StartX = start.X;
            StartY = start.Y;

            EndX = end.X;
            EndY = end.Y;
        }

        public int StartX { get; private set; }
        public int StartY { get; private set; }
        public int EndX { get; protected set; }
        public int EndY { get; protected set; }
    }
}
