﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace tsp.ViewModels.Geometry
{
    public class Drawable : INotifyPropertyChanged
    {
        public int Top { get; protected set; }
        public int Left { get; protected set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
