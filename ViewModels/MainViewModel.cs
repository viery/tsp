﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using tsp.Models;
using tsp.Models.Enums;

namespace tsp.ViewModels
{
    public class MainViewModel : ObservableObject
    {
        private static readonly Random random = new Random();
        private bool isRunning = false;

        public MainViewModel()
        {
            GenerateMapClicked = new RelayCommand(GenerateMap, CanGenerateMap);
            GeneratePopulationClicked = new RelayCommand(GeneratePopulation, CanGeneratePopulation);
            StartClicked = new RelayCommand(Start, CanStart);

            RoundTrips = new SortedSet<RoundTrip>();

            // some default values
            NodesMin = 20;
            NodesMax = 200;
            Nodes = 80;

            PopulationSize = 40;
            GenerationsMax = 8000;

            MutationChance = 0.30;
            MutationType = Mutation.invert;

            RecombinationChance = 0.25;
            RecombinationType = Recombination.order;

            EnableUserInputs(true);
        }

        #region GenerateMap
        private bool nodeNumberEnabled;
        private int nodes;
        
        public bool NodeNumberEnabled
        {
            get => nodeNumberEnabled;
            set => SetProperty(ref nodeNumberEnabled, value);
        }
        public int NodesMin { get; }

        public int NodesMax { get; }

        public int Nodes
        {
            get => nodes;
            set
            {
                if (nodes == value)
                {
                    return;
                }

                if (value < NodesMin || value > NodesMax)
                {
                    return;
                }

                nodes = value;

                OnPropertyChanged("Nodes");
            }
        }

        public RelayCommand GenerateMapClicked { get; private set; }
        
        private bool CanGenerateMap()
        {
            return !isRunning;
        }

        private void GenerateMap()
        {
            int mapBorder = 10; // TODO: get from Settings?
            int x = mapBorder;
            int y = mapBorder;
            int width = (int)(MapWidth - 2 * mapBorder);
            int height = (int)MapHeight - 2 * mapBorder;
            Rectangle area = new Rectangle(x, y, width, height);

            MasterRoundTrip = RoundTrip.RandomRoundTrip(area, Nodes);

            // Reset everything
            RoundTrips.Clear();
            Individual = 0;
            Generation = 0;
            Length = 0;

            // Draw a new Map with Lines only
            MapPoints = MasterRoundTrip;
            MapLines = null;

            UpdateButtonStatus();
        }
        #endregion

        #region GeneratePopulation
        private bool populationSizeEnabled;
        private bool generationsMaxEnabled;
        private int populationSize;
        private int generationsMax;

        public bool PopulationSizeEnabled
        {
            get => populationSizeEnabled;
            set => SetProperty(ref populationSizeEnabled, value);
        }

        public bool GenerationsMaxEnabled
        {
            get => generationsMaxEnabled;
            set => SetProperty(ref generationsMaxEnabled, value);
        }

        public int PopulationSize
        {
            get => populationSize;
            set
            {
                if (value != PopulationSize && value > 0)
                {
                    SetProperty(ref populationSize, value);
                }
            }
        }

        public int GenerationsMax
        {
            get => generationsMax;
            set
            {
                if (value != GenerationsMax && value > 0)
                {
                    SetProperty(ref generationsMax, value);
                }
            }
        }

        public RelayCommand GeneratePopulationClicked { get; private set; }

        private bool CanGeneratePopulation()
        {
            if (isRunning) return false;
            if (MasterRoundTrip == null) return false;

            return true;
        }

        private void GeneratePopulation()
        {
            // generate a new set of Roundtrips, based on our MasterRoundTrip
            RoundTrips.Clear();

            while (RoundTrips.Count < PopulationSize)
            {
                RoundTrip roundtrip = MasterRoundTrip.Randomize();
                RoundTrips.Add(roundtrip);
            }

            Generation = 0;
            Individual = RoundTrips.Count;
            Length = RoundTrips.First().Distance;

            MapLines = RoundTrips.First();

            UpdateButtonStatus();
        }
        #endregion

        #region Mutation
        private bool mutationTypeEnabled;
        private bool mutationChanceEnabled;
        private double mutationChance;

        public bool MutationTypeEnabled
        { 
            get => mutationTypeEnabled;
            set => SetProperty(ref mutationTypeEnabled, value);
        }

        public bool MutationChanceEnabled
        {
            get => mutationChanceEnabled;
            set => SetProperty(ref mutationChanceEnabled, value);
        }
 
        public double MutationChance
        {
            get => mutationChance;
            set => SetProperty(ref mutationChance, value);
        }

        public Mutation MutationType { get; set; }
        #endregion

        #region Recombination
        private double recombinationChance;
        private bool recombinationTypeEnabled;
        private bool recombinationChanceEnabled;

        public bool RecombinationTypeEnabled
        {
            get => recombinationTypeEnabled;
            set => SetProperty(ref recombinationTypeEnabled, value);
        }
        
        public bool RecombinationChanceEnabled
        {
            get => recombinationChanceEnabled;
            set => SetProperty(ref recombinationChanceEnabled, value);
        }
       
        public double RecombinationChance
        {
            get => recombinationChance;
            set => SetProperty(ref recombinationChance, value);
        }

        public Recombination RecombinationType { get; set; }
        #endregion

        #region Map
        private double mapWidth;
        private double mapHeight;
        private RoundTrip mapPoints;
        private RoundTrip mapLines;

        public double MapWidth
        {
            get => mapWidth;
            set => SetProperty(ref mapWidth, value);
        }

        public double MapHeight
        {
            get => mapHeight;
            set => SetProperty(ref mapHeight, value);
        }

        public RoundTrip MapPoints
        {
            get => mapPoints;
            set => SetProperty(ref mapPoints, value);
        }

        public RoundTrip MapLines
        {
            get => mapLines;
            set => SetProperty(ref mapLines, value);
        }
        #endregion

        #region StatusBar
        private int generation;
        private int individual;
        private double length;
        private double progress;

        public int Generation
        {
            get => generation;
            private set => SetProperty(ref generation, value);
        }

        public int Individual
        {
            get => individual;
            private set => SetProperty(ref individual, value);
        }

        public double Length
        {
            get => length;
            private set => SetProperty(ref length, value);
        }

        public double Progress
        {
            get => progress;
            private set => SetProperty(ref progress, value);
        }
        #endregion

        #region Start
        public RelayCommand StartClicked { get; set; }

        private bool CanStart()
        {
            if (isRunning) return false;
            if (MasterRoundTrip == null) return false;
            if (RoundTrips.Count == 0) return false;
            return true;
        }

        private async void Start()
        {
            isRunning = true;
            EnableUserInputs(false);

            Progress<StatusReportModel> progress = new Progress<StatusReportModel>();
            progress.ProgressChanged += StatusChanged;

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Console.WriteLine("Starting calculation ... ");

            Task task = Task.Run(() => RunEvolution(GenerationsMax, progress));
            await task;

            // Redraw the Map with the shortest RoundTrip
            MapLines = RoundTrips.First();

            stopWatch.Stop();

            TimeSpan ts = stopWatch.Elapsed;
            Console.WriteLine("done in {0:00}.{1:00}s", ts.TotalSeconds, ts.Milliseconds / 10);
       
            isRunning = false;
            UpdateButtonStatus();
            EnableUserInputs(true);
        }

        private void RunEvolution(int generationsMax, IProgress<StatusReportModel> progress)
        {
            StatusReportModel status = new StatusReportModel();

            for (int i = 0; i < generationsMax; i++)
            {
                // Generate new Individials for the new Generation
                List<RoundTrip> nextGeneration = GenerateNextGeneration(RoundTrips);

                // Add the new Individuals to the old Generation
                foreach (RoundTrip roundtrip in nextGeneration)
                {
                    RoundTrips.Add(roundtrip);
                }

                // Remove the worst Individuals from this Generation
                while (RoundTrips.Count > PopulationSize)
                {
                    RoundTrips.Remove(RoundTrips.Last());
                }

                // Update the Status ... 
                status.Generation = 1;
                status.Individual = nextGeneration.Count;
                status.Progress = (i + 1) * 100 / generationsMax;
                status.BestRoundTrip = RoundTrips.First();

                // ... and report back
                progress.Report(status);
            }
        }
        #endregion

        #region Evolution
        private RoundTrip MasterRoundTrip { get; set; }

        private SortedSet<RoundTrip> RoundTrips { get; }

        private List<RoundTrip> GenerateNextGeneration(ISet<RoundTrip> parentGeneration)
        {
            List<RoundTrip> nextGeneration = new List<RoundTrip>();
            RoundTrip roundTrip;

            foreach (RoundTrip parent in parentGeneration)
            {
                roundTrip = RecombinateIndividual(parent);
                if (roundTrip != null)
                {
                    nextGeneration.Add(roundTrip);
                }

                roundTrip = MutateIndividual(parent);
                if (roundTrip != null)
                {
                    nextGeneration.Add(roundTrip);
                }
            }

            return nextGeneration;
        }

        private RoundTrip RecombinateIndividual(RoundTrip parent)
        {
            if (random.NextDouble() < RecombinationChance)
            {
                int i = random.Next(RoundTrips.Count);
                RoundTrip parentA = RoundTrips.ElementAt(i);
                RoundTrip parentB = parent;

                RoundTrip child = RoundTrip.Recombinate(parentA, parentB, RecombinationType);
                return child;
            }

            return null;
        }

        private RoundTrip MutateIndividual(RoundTrip parent)
        {
            if (random.NextDouble() < MutationChance)
            {
                RoundTrip child = RoundTrip.Mutate(parent, MutationType);
                return child;
            }

            return null;
        }

        private void StatusChanged(object sender, StatusReportModel statusReport)
        {
            Generation += statusReport.Generation;
            Individual += statusReport.Individual;
            Progress = statusReport.Progress;
            Length = statusReport.BestRoundTrip.Distance;

            MapLines = statusReport.BestRoundTrip;
        }
        #endregion

        #region Misc
        private void UpdateButtonStatus()
        {
            GeneratePopulationClicked.NotifyCanExecuteChanged();
            GenerateMapClicked.NotifyCanExecuteChanged();
            StartClicked.NotifyCanExecuteChanged();
        }

        private void EnableUserInputs(bool isEnabled)
        {
            NodeNumberEnabled = isEnabled;

            PopulationSizeEnabled = isEnabled;
            GenerationsMaxEnabled = isEnabled;

            MutationTypeEnabled = isEnabled;
            MutationChanceEnabled = isEnabled;

            RecombinationTypeEnabled = isEnabled;
            RecombinationChanceEnabled = isEnabled;
        }
        #endregion
    }
}
