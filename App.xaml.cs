﻿using System.Windows;
using tsp.ViewModels;
using tsp.Views;
using tsp.Views.Widgets;

namespace tsp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainView mainView = new MainView();
            MainViewModel mainViewModel = new MainViewModel();

            mainView.DataContext = mainViewModel;
            mainView.Show();
        }

    }
}
