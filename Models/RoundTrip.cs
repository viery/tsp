﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using tsp.Models.Enums;

namespace tsp.Models
{
    public class RoundTrip : IComparable<RoundTrip>, IEnumerable<Node>
    {
        private static readonly Random random = new Random();
        private readonly List<Node> nodes;

        public static RoundTrip RandomRoundTrip(Rectangle rectangle, int count)
        {
            List<Node> nodes = new List<Node>();

            while (nodes.Count < count)
            {
                int x = random.Next(rectangle.X, rectangle.Width);
                int y = random.Next(rectangle.Y, rectangle.Height);

                Node node = new Node(x, y);

                if (!nodes.Contains(node))
                {
                    nodes.Add(new Node(x, y));
                }
            }

            return new RoundTrip(nodes);
        }

        public static RoundTrip Recombinate(RoundTrip a, RoundTrip b, Recombination type)
        {
            switch (type)
            {
                case Recombination.edge: return EdgeRecombination(a, b);
                case Recombination.order: return OrderRacombinate(a, b);
            }

            throw new ArgumentOutOfRangeException("type");
        }

        public static RoundTrip Mutate(RoundTrip roundTrip, Mutation type)
        {
            switch (type)
            {
                case Mutation.invert: return InvertMutate(roundTrip);
                case Mutation.swap: return SwapMutate(roundTrip);
            }

            throw new ArgumentOutOfRangeException("Type");
        }

        public RoundTrip Randomize()
        {
            List<Node> randomNodeList = nodes.OrderBy(item => random.Next()).ToList();

            return new RoundTrip(randomNodeList);
        }

        public double Distance { get; }

        public int Count { get => nodes.Count; }

        #region Private Methods
        private RoundTrip(List<Node> nodes)
        {
            this.nodes = new List<Node>(nodes);

            // Calculate Distance
            Node start = this.nodes.Last();
            foreach (Node destination in this.nodes)
            {
                Distance += Node.Distance(start, destination);
                start = destination; //set the startpoint for the next segment
            }
        }

        private static RoundTrip OrderRacombinate(RoundTrip first, RoundTrip second)
        {
            if (first.Count != second.Count) { throw new ArgumentException(); }

            int count = first.Count;
            int pivot = random.Next(first.Count);
            List<Node> nodes = new List<Node>(first.nodes);

            nodes.RemoveRange(pivot, count - pivot);                    // keep "pivot" Nodes from "first"
            nodes.AddRange(second.Where(x => !nodes.Any(y => x == y))); // and add the other from "second"

            return new RoundTrip(nodes);
        }

        private static RoundTrip EdgeRecombination(RoundTrip a, RoundTrip b)
        {
            //TODO
            return a;
        }

        private static RoundTrip InvertMutate(RoundTrip roundTrip)
        {
            List<Node> nodes = new List<Node>(roundTrip.nodes);

            int a = random.Next(0, nodes.Count);
            int b = random.Next(a, nodes.Count);

            for (int i = a; i <= b; i++)
            {
                nodes[a + b - i] = roundTrip.nodes[i];
            }

            return new RoundTrip(nodes);
        }

        private static RoundTrip SwapMutate(RoundTrip roundTrip)
        {
            List<Node> nodes = new List<Node>(roundTrip.nodes);

            int a = random.Next(nodes.Count);
            int b = random.Next(nodes.Count);

            Node tmp = nodes[a];
            nodes[a] = nodes[b];
            nodes[b] = tmp;

            return new RoundTrip(nodes);
        }
        #endregion

        #region ICompareable
        public int CompareTo(RoundTrip other)
        {
            return (int)(Distance - other.Distance);
        }
        #endregion

        #region IEnumerable
        public IEnumerator<Node> GetEnumerator()
        {
            return nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return nodes.GetEnumerator();
        }
        #endregion
    }
}
