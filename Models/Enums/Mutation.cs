﻿using System;
using System.ComponentModel;

namespace tsp.Models.Enums
{
    public enum Mutation
    {
        [Description("Inverting")]
        invert,

        [Description("Swap")]
        swap
    }
}