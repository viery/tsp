﻿using System.ComponentModel;

namespace tsp.Models.Enums
{
    public enum Recombination
    {
        [Description("Edge")]
        edge,

        [Description("Order")]
        order
    }
}
