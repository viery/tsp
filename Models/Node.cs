﻿using System;

namespace tsp.Models
{
    public class Node : IEquatable<Node>
    {
        public const int minDistance = 10;

        public Node(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public double Distance(Node to)
        {
            return Distance(this, to);
        }

        public static double Distance(Node from, Node to)
        {
            int dx = from.X - to.X;
            int dy = from.Y - to.Y;

            return Math.Sqrt(dx * dx + dy * dy);
        }

        public bool Equals(Node other)
        {
            int xMin = other.X - minDistance;
            int xMax = other.X + minDistance;
            int yMin = other.Y - minDistance;
            int yMax = other.Y + minDistance;

            //if out of Range in X
            if (xMin >= X || X >= xMax)
            {
                return false;
            }

            //if out of Range in Y
            if (yMin >= Y || Y >= yMax)
            {
                return false;
            }

            return true;
        }
    }
}
