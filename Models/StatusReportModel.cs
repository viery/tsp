﻿namespace tsp.Models
{
    public class StatusReportModel
    {
        public int Generation { get; set; }
        public int Individual { get; set; }
        public int Progress { get; set; }
        public RoundTrip BestRoundTrip { get; set; }

        public StatusReportModel()
        {

        }
    }
}
