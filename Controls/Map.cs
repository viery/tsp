﻿using Microsoft.Toolkit.Mvvm.Input;
using ModernWpf;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using tsp.Models;
using Windows.UI.ViewManagement;

namespace tsp.Controls
{
    public class Map : Canvas
    {
        public static readonly DependencyProperty MapWidthProperty =
            DependencyProperty.RegisterAttached("MapWidth", typeof(double), typeof(Map));

        public static readonly DependencyProperty MapHeightProperty =
            DependencyProperty.RegisterAttached("MapHeight", typeof(double), typeof(Map));

        public static readonly DependencyProperty MapPointsProperty =
            DependencyProperty.RegisterAttached("MapPoints", typeof(RoundTrip), typeof(Map), new PropertyMetadata(OnMapChanged));

        public static readonly DependencyProperty MapLinesProperty =
            DependencyProperty.RegisterAttached("MapLines", typeof(RoundTrip), typeof(Map), new PropertyMetadata(OnMapChanged));

        public static readonly DependencyProperty MapRedrawCommandProperty =
            DependencyProperty.RegisterAttached("MapRedrawCommand", typeof(RelayCommand), typeof(Map));

        private static Brush PointFillBrush { get; } = new SolidColorBrush();
        private static Pen PointBorderPen { get; } = new Pen(SystemParameters.WindowGlassBrush, 2);
        private static Pen LineBorderPen { get; } = new Pen(new SolidColorBrush(Colors.Gray), 1);

        public Map()
        {
            SizeChanged += OnMapSizeChanged;
        }

        #region Size
        public double MapWidth
        {
            get => (double)GetValue(MapWidthProperty);
            set => SetValue(MapWidthProperty, value);
        }

        public double MapHeight
        {
            get => (double)GetValue(MapHeightProperty);
            set => SetValue(MapHeightProperty, value);
        }

        private void OnMapSizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateMapSize((FrameworkElement)sender);
        }

        private void UpdateMapSize(FrameworkElement frameworkElement)
        {
            frameworkElement.SetCurrentValue(MapWidthProperty, frameworkElement.ActualWidth);
            frameworkElement.SetCurrentValue(MapHeightProperty, frameworkElement.ActualHeight);
        }
        #endregion

        public RoundTrip MapPoints
        {
            get => (RoundTrip)GetValue(MapPointsProperty);
            set => SetValue(MapPointsProperty, value);
        }

        public RoundTrip MapLines
        {
            get => (RoundTrip)GetValue(MapLinesProperty);
            set => SetValue(MapLinesProperty, value);
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            if (MapPoints?.Count > 0)
            {
                foreach (Node point in MapPoints)
                {
                    dc.DrawEllipse(PointFillBrush, PointBorderPen, new Point(point.X, point.Y), 3, 3);
                }
            }

            if (MapLines?.Count > 0)
            {
                Node start = MapLines.Last();
                foreach (Node line in MapLines)
                {
                    dc.DrawLine(LineBorderPen, new Point(start.X, start.Y), new Point(line.X, line.Y));
                    start = line;
                }
            }
        }

        private static void OnMapChanged(DependencyObject sender, DependencyPropertyChangedEventArgs eventsArgs)
        {
            Map map = sender as Map;
            map?.InvalidateVisual();
        }
    }
}
